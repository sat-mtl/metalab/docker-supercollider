### Docker image of SuperCollider

This image is intended to be used for continuous integration. It is based on Ubuntu 20.04, and SuperCollider is built headless. This image includes sc3-plugins and a JACK1 server running in dummy mode.

Docker Hub: https://hub.docker.com/r/metalabsat/supercollider

#### Build and Deploy image

1. `cd` into the folder containing the `Dockerfile`
2. Run `docker build --tag <your-username>/supercollider:<version> .`
3. Run `docker login` in order to have the rights to push the image to Docker Hub
4. Run `docker push <your-username>/supercollider:<version>` to push the image
5. Run `docker logout` when finished
